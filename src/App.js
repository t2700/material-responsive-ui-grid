import logo from "./logo.svg";
import "./App.css";
import {Stack,Box, createTheme,Grid,styled,Paper } from "@mui/material";
import Sidebar from "./components/Sidebar";
import Feed from "./components/Feed";
import Rightbar from "./components/Rightbar";
import Navbar from "./components/Navbar";
import Add from "./components/Add"
import { useState } from "react";
import { ThemeProvider } from "@emotion/react";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Pages from "./components/Pages";
import Dashboard from "./components/Dashboard";
import MarketPlaces from "./components/MarketPlace";
import Settings from "./components/Settings";
import Profile from "./components/Profile";
import Groups from "./components/Groups";
import Friends from "./components/Friends";
 

function App() {
 
 

  return (
    
      <Router>
    {/* <Dashboard/> */}
            <Routes>
              <Route exact path="/" element={<Dashboard/>}/>
                <Route exact path="/pages" element={<Pages/>}/>
                <Route exact path="/group" element={<Groups/>}/>
                <Route exact path="/market" element={<MarketPlaces/>}/>
                <Route exact path="/friend" element={<Friends/>}/>
                <Route exact path="/settings" element={<Settings/>}/>
                <Route exact path="/profile" element={<Profile/>}/>
                {/* <Route exact path="/pages" element={<Pages/>}/>
                <Route exact path="/pages" element={<Pages/>}/> */}
             </Routes>
        </Router>
  );
}

export default App;
