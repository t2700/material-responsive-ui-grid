import React from "react";
import { useState } from "react";
import { Stack, Box, createTheme, Grid, styled, Paper } from "@mui/material";
import Sidebar from "./Sidebar";
import Feed from "./Feed";
import Rightbar from "./Rightbar";
import Navbar from "./Navbar";
import Add from "./Add";
import { ThemeProvider } from "@emotion/react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

function Dashboard() {
  const [mode, setMode] = useState("light");
  const darkTheme = createTheme({
    palette: {
      mode: mode,
    },
  });

  return (
    <ThemeProvider theme={darkTheme}>
      <Box bgcolor={"background.default"} color={"text.primary"}>
        <Navbar />
        <Grid container spacing={2}>
          <Grid item xs={6} md={2} sm={0}>
            <Sidebar setMode={setMode} mode={mode} />
          </Grid>
          <Grid item xs={12} md={6} sm={12}>
            <Feed />
          </Grid>
          <Grid item xs={12} md={4} sm={12}>
            <Rightbar />
          </Grid>
        </Grid>
        <Add />
      </Box>
    </ThemeProvider>
  );
}

export default Dashboard;
