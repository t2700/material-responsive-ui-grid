import React,{useState} from 'react';
import {
    Typography,
    IconButton,
    CardMedia,
    Box,
    Card,
    CardHeader,
    Avatar,
    CardActions,
    CardContent,
    Checkbox
  } from "@mui/material";
  import { Favorite,Share,FavoriteBorder,MoreVert } from "@mui/icons-material";
  

export default function Post() {
  const [mode,setMode]= useState("light");

  return (
    
        <Card sx={{margin:5,marginLeft:10}}>
        <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: "red" }} aria-label="recipe">
              Y
            </Avatar>
          }
          action={
            <IconButton aria-label="settings">
              <MoreVert />
            </IconButton>
          }
          title="Yashaswini K"
          subheader="September 14, 2022"
        />
        <CardMedia
          component="img"
          height="20%"
          image="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGRR41a5aSN_sLAqEeQfhjIWuVmovBy1PoiA&usqp=CAU"
          alt="Paella dish"
        />
        <CardContent>
          <Typography variant="body2" color="text.secondary">
            This impressive paella is a perfect party dish and a fun meal to
            cook together with your guests. Add 1 cup of frozen peas along with
            the mussels, if you like.
          </Typography>
        </CardContent>
        <CardActions disableSpacing>
          <IconButton aria-label="add to favorites">
           <Checkbox icon={<FavoriteBorder/>} checkedIcon={<Favorite sx={{color:"red"}}/>}/>
          </IconButton>
          <IconButton aria-label="share">
            <Share />
          </IconButton>
        </CardActions>
      </Card>
  
  )
}
