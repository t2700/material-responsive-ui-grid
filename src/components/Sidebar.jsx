import React from "react";
import {
  Box,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  ListItemIcon,
  Switch
} from "@mui/material";
import {Home,AccountBox,ModeNight,Storefront,Person,Settings,Pages,Groups} from "@mui/icons-material";
import { Link } from "react-router-dom";

export default function Sidebar({setMode,mode}) {
  return (
    <Box
      // flex={1}
      p={2}
    >
      <Box position="fixed">
      <List>
        <ListItem disablePadding>
          <ListItemButton component="a">
          <Link to="/">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Home/>
            </ListItemIcon>
            </Link> 
            <ListItemText primary="HomePage" sx={{ display: { xs: "none", sm: "none" ,md:"block"} }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a">
          <Link to="/pages">
            <ListItemIcon sx={{ display:  { xs: "none", sm: "none" ,md:"block"} }}>
            <Pages />
            </ListItemIcon>
            </Link> 
            <ListItemText primary="Pages" sx={{ display:  { xs: "none", sm: "none" ,md:"block"} }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a">
          <Link to="/group">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Groups />
            </ListItemIcon>
            </Link>
            <ListItemText primary="Groups" sx={{ display: { xs: "none", sm: "none" ,md:"block"} }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" >
          <Link to="/market">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Storefront />
            </ListItemIcon>
            </Link>
            <ListItemText primary="Marketplace" sx={{ display:  { xs: "none", sm: "none" ,md:"block"} }}/>
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" >
          <Link to="/friend">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Person />
            </ListItemIcon>
            </Link>
            <ListItemText primary="Friends" sx={{ display:  { xs: "none", sm: "none" ,md:"block"} }}/>
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" >
          <Link to="/settings">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <Settings />
            </ListItemIcon>
            </Link>
            <ListItemText  primary="Settings" sx={{ display: { xs: "none", sm: "none" ,md:"block"} }} />
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a">
          <Link to="/profile">
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <AccountBox />
            </ListItemIcon>
            </Link>
            <ListItemText primary="Profile"  sx={{ display:  { xs: "none", sm: "none" ,md:"block"} }}/>
          </ListItemButton>
        </ListItem>
        <ListItem disablePadding>
          <ListItemButton component="a" >
            <ListItemIcon sx={{ display: { xs: "block", sm: "block" } }}>
              <ModeNight />
            </ListItemIcon>
          <Switch onChange={e=>setMode(mode ==="light"?"dark":"light")} />
          </ListItemButton>
        </ListItem>
      </List>
      </Box>
    </Box>
  );
}
