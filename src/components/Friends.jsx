import { Box, Grid } from "@mui/material";
import React, { useState } from "react";
import Navbar from "./Navbar";
import Sidebar from "./Sidebar";

function Friends() {
  const [mode, setMode] = useState("light");

  return (
    <Box bgcolor={"background.default"} color={"text.primary"}>
      <Navbar />
      <Grid container spacing={2}>
        <Grid item xs={6} md={2} sm={0}>
          <Sidebar setMode={setMode} mode={mode} />
        </Grid>
        <Grid item xs={12} md={6} sm={12}>
         Friends
        </Grid>
        <Grid item xs={12} md={4} sm={12}>
          {/* <Rightbar/>  */}
        </Grid>
      </Grid>
      {/* <Add/> */}
    </Box>
  );
}

export default Friends;
