import React from "react";
import {
  Stack,
  ImageListItem,
  ImageList,
  Box,
  Typography,
  AvatarGroup,
  Avatar,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from "@mui/material";

export default function Rightbar() {
  return (
    <Box 
    // flex={2} 
     height="100vh"
     sx={{margin:5,marginLeft:10}}
    p={2}
    //  sx={{ display: { xs: "none", sm: "block" } }}
     >
      <Box position="none" width="100%">
        <Typography variant="h6" fontWeight={100}>
          Online Friends
        </Typography>
        <AvatarGroup max={7}>
          <Avatar
            alt="Remy Sharp"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKKezWBInJQJnKrkPdxfEyWH9kIITVoQDaBw&usqp=CAU"
          />
          <Avatar
            alt="Travis Howard"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRMmw7nHXVUPzHbVf1CNv8SQeFQJutx45PN_A&usqp=CAU"
          />
          <Avatar
            alt="Cindy Baker"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ2TRmnlIPn36w1HuBoXo7mreaRsINRXlhFWg&usqp=CAU"
          />
          <Avatar
            alt="Agnes Walker"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSqj2L5d6mookzyksdqyk2cPO5zFyazCuuYgA&usqp=CAU"
          />
          <Avatar
            alt="Trevor Henderson"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR1Ck9Phis9Jz_iAEWo91LIDteg4ktqin-dkw&usqp=CAU"
          />
          <Avatar
            alt="Remy Sharp"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOtRcjBt3StgtQ1GONzPVOQGfG9wM0K-Oh7w&usqp=CAU"
          />
          <Avatar
            alt="Travis Howard"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQZ-VZvODeKWx7b-O3L8TBWwDfWBoIr2XGlNA&usqp=CAU"
          />
          <Avatar
            alt="Cindy Baker"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLnxcOHaobrSP19eZy1K-YXwEn3GJAf49t0g&usqp=CAU"
          />
          <Avatar
            alt="Agnes Walker"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDpx5VCnq7kGt9oegDeYb-Ol6DmzxAGKp7gw&usqp=CAU"
          />
          <Avatar
            alt="Trevor Henderson"
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLnxcOHaobrSP19eZy1K-YXwEn3GJAf49t0g&usqp=CAU"
          />
        </AvatarGroup>
        <Typography variant="h6" fontWeight={100} mt={2} mb={2}>
          Latest Photos
        </Typography>
        <ImageList cols={3} rowHeight={100} gap={5}>
          <ImageListItem>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROB9PzURPg_4x_d7RaPYvOwCz7Tt8TkdODlg&usqp=CAU"
              alt=""
            />
          </ImageListItem>
          <ImageListItem>
            <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREHzWxfj_pT1STbQhpXs9Yo7GoEgQgquM7QA&usqp=CAU"
              alt=""
            />
          </ImageListItem>
          <ImageListItem>
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStBcADfwp39lnVZ3pX1BlDhy1Q-4pO3Urndw&usqp=CAU"
              alt=""
            />
          </ImageListItem>
          
        </ImageList>
        <Typography variant="h6" fontWeight={100} mt={2} mb={2}>
          Latest Conversation
        </Typography>
        <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Remy Sharp" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOmzGJSRmK5OzPN_e7tO8WJbLn3aIXksw0Jg&usqp=CAU" />
        </ListItemAvatar>
        <ListItemText
          primary="Brunch this weekend?"
          secondary={
            <React.Fragment>
              <Typography
                sx={{ display: 'inline' }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                Ali Connors
              </Typography>
              {" — I'll be in your neighborhood doing errands this…"}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Travis Howard" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnLnYezOHk0pityoQYbxo-mau2N-ISzNr9GQ&usqp=CAU" />
        </ListItemAvatar>
        <ListItemText
          primary="Summer BBQ"
          secondary={
            <React.Fragment>
              <Typography
                sx={{ display: 'inline' }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                to Scott, Alex, Jennifer
              </Typography>
              {" — Wish I could come, but I'm out of town this…"}
            </React.Fragment>
          }
        />
      </ListItem>
      <Divider variant="inset" component="li" />
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Cindy Baker" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_60ifnzVNSe0Xadl-2wGQ-YVja1IVdKSzWQ&usqp=CAU" />
        </ListItemAvatar>
        <ListItemText
          primary="Oui Oui"
          secondary={
            <React.Fragment>
              <Typography
                sx={{ display: 'inline' }}
                component="span"
                variant="body2"
                color="text.primary"
              >
                Sandra Adams
              </Typography>
              {' — Do you have Paris recommendations? Have you ever…'}
            </React.Fragment>
          }
        />
      </ListItem>
    </List>
      </Box>
    </Box>
  );
}
